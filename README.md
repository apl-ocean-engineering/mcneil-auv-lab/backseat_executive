## backseat_executive

Responsible for all high-level interactions with the stretch limo payload
and its interactions with the front seat.
This includes:
* monitor pressure and leak sensors for possible leak; will command an abort if leak detected or no data received from these sensors.
* turn on pump only when in mission mode
