/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INCLUDE_BACKSEAT_EXECUTIVE_BACKSEAT_EXECUTIVE_H_
#define INCLUDE_BACKSEAT_EXECUTIVE_BACKSEAT_EXECUTIVE_H_

#include <ros/ros.h>

#include <string>

#include "apl_msgs/LeakDetection.h"
#include "apl_msgs/PowerCommand.h"
#include "apl_msgs/PowerStatus.h"
#include "remus_backseat_msgs/R2VAbortMission.h"
#include "remus_backseat_msgs/V2RState.h"
#include "sensor_msgs/FluidPressure.h"

class BackseatExecutive {
 public:
  BackseatExecutive();
  ~BackseatExecutive() = default;

  void run();

 private:
  void setupParams();

  void stateCallback(const remus_backseat_msgs::V2RState& msg);
  void powerStatusCallback(const apl_msgs::PowerStatus& msg);

  void leakDataCallback(const apl_msgs::LeakDetection& msg);
  void pressureDataCallback(const sensor_msgs::FluidPressure& msg);

  void leakTimerCallback(const ros::TimerEvent& event);
  void pressureTimerCallback(const ros::TimerEvent& event);

  // Helper function that sends an AbortMission message
  void requestAbort();
  // Manage pump power channel, based on in-mission status and desired
  // cycling parameters.
  void handlePump(int mode);
  // Helper function that sends a PowerCommand message
  void publishPumpCommand(bool command);

  ros::NodeHandle nh_;
  // Subscribes to Recon state messages
  ros::Subscriber state_sub_;
  // monitors vacuum
  ros::Subscriber pressure_sub_;
  // monitors leak detector
  ros::Subscriber leak_sub_;
  // monitors current status of power manager
  ros::Subscriber power_status_sub_;
  // manages the power control for the SBE 5T pump
  ros::Publisher power_command_pub_;
  // Commands an abort if necessary
  ros::Publisher abort_pub_;
  // Timer that will trigger if leak messages aren't received
  ros::Timer leak_timer_;
  // Timer that will trigger if pressure messages aren't received
  ros::Timer pressure_timer_;

  // Pressure above which the remus will abort. Pascals.
  float maximum_pressure_;
  // The executive will trigger an abort after not receiving a pressure
  // command in this long.
  float leak_abort_timeout_;
  float pressure_abort_timeout_;

  ///// Section managing the pump
  // If pump not installed, none of the following parameters are required.
  bool pump_installed_;
  // Power channel that controls the pump and should be turned on during the
  // mission.
  std::string pump_power_channel_;
  // Most recently received status for the pump power.
  // int so -1 can mean not-yet-received.
  int pump_power_status_;
  // How long the pump power cycling period is
  int pump_period_secs_;
  // How long to stay turned off
  int pump_off_secs_;
};

#endif  // INCLUDE_BACKSEAT_EXECUTIVE_BACKSEAT_EXECUTIVE_H_
