/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "backseat_executive/backseat_executive.h"

#include <ros/ros.h>

#include "apl_msgs/LeakDetection.h"
#include "apl_msgs/PowerCommand.h"
#include "apl_msgs/PowerStatus.h"
#include "remus_backseat_msgs/R2VAbortMission.h"
#include "remus_backseat_msgs/V2RState.h"
#include "sensor_msgs/FluidPressure.h"

BackseatExecutive::BackseatExecutive() {
  nh_ = ros::NodeHandle("~");
  setupParams();
  pump_power_status_ = -1;

  abort_pub_ = nh_.advertise<remus_backseat_msgs::R2VAbortMission>(
      "R2VAbortMission", 10);

  power_command_pub_ =
      nh_.advertise<apl_msgs::PowerCommand>("power_command", 10);

  state_sub_ =
      nh_.subscribe("V2RState", 1, &BackseatExecutive::stateCallback, this);
  pressure_sub_ = nh_.subscribe("pressure", 1,
                                &BackseatExecutive::pressureDataCallback, this);
  leak_sub_ =
      nh_.subscribe("leak", 1, &BackseatExecutive::leakDataCallback, this);
  power_status_sub_ = nh_.subscribe(
      "power_status", 1, &BackseatExecutive::powerStatusCallback, this);

  leak_timer_ =
      nh_.createTimer(ros::Duration(leak_abort_timeout_),
                      &BackseatExecutive::leakTimerCallback, this, true);
  pressure_timer_ =
      nh_.createTimer(ros::Duration(pressure_abort_timeout_),
                      &BackseatExecutive::pressureTimerCallback, this, true);
}

void BackseatExecutive::setupParams() {
  // Housing pressure above which we trigger an abort
  nh_.getParam("maximum_pressure", maximum_pressure_);
  // In addition to arborting the mission due to a leak, we will abort if we
  // haven't received data from the leak sensors.
  nh_.getParam("leak_abort_timeout", leak_abort_timeout_);
  nh_.getParam("pressure_abort_timeout", pressure_abort_timeout_);

  // For backwards compatability, default to true.
  nh_.param<bool>("pump_installed", pump_installed_, true);
  if (pump_installed_) {
    // e.g. "SBE 5T"
    nh_.getParam("pump_power_channel", pump_power_channel_);
    // Pump power cycling, requested by Craig to enforce resets.
    // length of cycle
    nh_.getParam("pump_period_secs", pump_period_secs_);
    // how long pump is off during each cycle
    nh_.getParam("pump_off_secs", pump_off_secs_);
  }
}

void BackseatExecutive::stateCallback(
    const remus_backseat_msgs::V2RState& msg) {
  if (pump_installed_) {
    handlePump(msg.mode);
  }
}

void BackseatExecutive::handlePump(int mode) {
  bool mode_pump_command = false;
  // First, figure out whether we are in a mission and thus should be
  // running the pump.
  switch (mode & 0b11) {
    case 0b00:
      mode_pump_command = false;
      break;
    case 0b01:
      mode_pump_command = true;
      break;
    case 0b10:
      mode_pump_command = true;
      break;
    case 0b11:
      mode_pump_command = false;
      break;
  }
  ROS_INFO_STREAM("Based on vehicle state "
                  << mode << ", pump command is: " << mode_pump_command);
  // Next, if the pump is on during the mission, determine whether it should
  // be turned off for the periodic resets.
  int tt = ros::Time::now().sec;
  bool cycle_pump_command = true;
  int timer_mod = tt % pump_period_secs_;
  if (timer_mod < pump_off_secs_) {
    ROS_INFO_STREAM("...pump should be OFF for power cycling");
    cycle_pump_command = false;
  } else {
    ROS_INFO_STREAM("...pump should be ON for power cycling");
  }

  bool pump_cmd = mode_pump_command & cycle_pump_command;
  if (!pump_cmd && pump_power_status_ != 0) {
    ROS_WARN_STREAM("Turning pump off. Mode = "
                    << mode << ", Timer = " << timer_mod
                    << "(period = " << pump_period_secs_
                    << ", off = " << pump_off_secs_ << ")");
    publishPumpCommand(false);
  } else {
    ROS_INFO_STREAM("Pump already off; no need to send another command");
  }

  if (pump_cmd && pump_power_status_ != 1) {
    ROS_WARN_STREAM("Turning pump on. Mode = "
                    << mode << ", Timer = " << timer_mod
                    << "(period = " << pump_period_secs_
                    << ", off = " << pump_off_secs_ << ")");
    publishPumpCommand(true);
  } else {
    ROS_INFO_STREAM("Pump already on; no need to send another command");
  }
}

void BackseatExecutive::powerStatusCallback(const apl_msgs::PowerStatus& msg) {
  // None of these are fatal errors, because we don't want the Executive
  // to simply quit (need it to run in case of leaks!)
  if (msg.channels.size() != msg.status.size()) {
    ROS_ERROR_STREAM_ONCE("Received PowerStatus with "
                          << msg.channels.size() << " channels, and "
                          << msg.status.size()
                          << " status flags. This should match!");
    return;
  }
  if (pump_installed_) {
    bool pump_power_matched = false;
    for (int ii = 0; ii < msg.channels.size(); ii++) {
      if (msg.channels[ii] == pump_power_channel_) {
        pump_power_status_ = msg.status[ii];
        pump_power_matched = true;
        break;
      }
    }
    if (!pump_power_matched) {
      ROS_ERROR_STREAM_ONCE(
          "Recieved PowerStatus that didn't have data for "
          << pump_power_channel_
          << ", which is the channel the Executive should be tracking.");
    }
  }
}

void BackseatExecutive::pressureDataCallback(
    const sensor_msgs::FluidPressure& msg) {
  if (msg.fluid_pressure > maximum_pressure_) {
    ROS_ERROR_STREAM(
        "Executive detected too-high pressure; sending abort command");
    requestAbort();
  }
  // Pet the pressure watchdog
  pressure_timer_.stop();
  pressure_timer_.setPeriod(ros::Duration(leak_abort_timeout_));
  pressure_timer_.start();
}

void BackseatExecutive::leakDataCallback(const apl_msgs::LeakDetection& msg) {
  for (const auto& ll : msg.leak) {
    if (ll) {
      ROS_ERROR_STREAM("Executive detected a leak; sending abort command");
      requestAbort();
    }
  }
  // Pet the leak watchdog
  leak_timer_.stop();
  leak_timer_.setPeriod(ros::Duration(leak_abort_timeout_));
  leak_timer_.start();
}

void BackseatExecutive::leakTimerCallback(const ros::TimerEvent& event) {
  ROS_ERROR_STREAM("Executive has not received a leak message in "
                   << leak_abort_timeout_ << " seconds. Sending abort command");
  requestAbort();
  // Go ahead and reset timer, just in case that message got dropped, we'll
  // re-send an abort QUESTION: Is it better to send abort requests at a fixed
  // rate, in a main loop?
  leak_timer_.stop();
  leak_timer_.setPeriod(ros::Duration(leak_abort_timeout_));
  leak_timer_.start();
}

void BackseatExecutive::pressureTimerCallback(const ros::TimerEvent& event) {
  ROS_ERROR_STREAM("Executive has not received a pressure message in "
                   << pressure_abort_timeout_
                   << " seconds. Sending abort command");
  requestAbort();
  // Go ahead and reset timer, just in case that message got dropped, we'll
  // re-send an abort QUESTION: Is it better to send abort requests at a fixed
  // rate, in a main loop?
  pressure_timer_.stop();
  pressure_timer_.setPeriod(ros::Duration(leak_abort_timeout_));
  pressure_timer_.start();
}

void BackseatExecutive::requestAbort() {
  remus_backseat_msgs::R2VAbortMission abort_msg;
  abort_msg.header.stamp = ros::Time::now();
  abort_pub_.publish(abort_msg);
}

void BackseatExecutive::publishPumpCommand(bool command) {
  apl_msgs::PowerCommand power_msg;
  power_msg.header.stamp = ros::Time::now();
  power_msg.channels.push_back(pump_power_channel_);
  if (command) {
    power_msg.commands.push_back(apl_msgs::PowerCommand::ON);
  } else {
    power_msg.commands.push_back(apl_msgs::PowerCommand::OFF);
  }
  power_command_pub_.publish(power_msg);
}

void BackseatExecutive::run() { ros::spin(); }

int main(int argc, char** argv) {
  ros::init(argc, argv, "backseat_executive");
  BackseatExecutive ex;
  ex.run();
  return 0;
}
