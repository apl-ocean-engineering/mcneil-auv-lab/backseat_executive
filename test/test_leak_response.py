#! /usr/bin/env python3

import numpy as np
import rospy
import unittest

import apl_msgs.msg
import remus_backseat_msgs.msg
import sensor_msgs.msg


class TestLeakResponse(unittest.TestCase):
    def setUp(self):
        # Seconds to sleep while waiting for ROS messages to be sent/received
        self.rosmsg_delay = 0.2
        self.abort_msg = None
        self.abort_sub = rospy.Subscriber(
            "/executive/R2VAbortMission",
            remus_backseat_msgs.msg.R2VAbortMission,
            self.abort_callback,
        )
        self.pressure_pub = rospy.Publisher(
            "/executive/pressure", sensor_msgs.msg.FluidPressure, queue_size=1
        )
        self.leak_pub = rospy.Publisher(
            "/executive/leak", apl_msgs.msg.LeakDetection, queue_size=1
        )

    def abort_callback(self, msg):
        self.abort_msg = msg

    def publish_pressure(self, pressure: float) -> None:
        msg = sensor_msgs.msg.FluidPressure()
        msg.header.stamp = rospy.Time.now()
        msg.fluid_pressure = pressure
        self.pressure_pub.publish(msg)

    def publish_leak(self, leak: bool) -> None:
        msg = apl_msgs.msg.LeakDetection()
        msg.header.stamp = rospy.Time.now()
        msg.leak = [leak]
        self.leak_pub.publish(msg)

    def test_leak_detected(self):
        self.assertIsNone(self.abort_msg)
        for _ in range(3):
            self.publish_leak(False)
            rospy.sleep(self.rosmsg_delay)
            self.assertIsNone(self.abort_msg)
        self.publish_leak(True)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.abort_msg)

    def test_pressure_high(self):
        pressure_threshold = rospy.get_param("/executive/maximum_pressure")
        self.assertIsNone(self.abort_msg)
        for _ in range(3):
            pressure = pressure_threshold * np.random.random()
            self.publish_pressure(pressure)
            rospy.sleep(self.rosmsg_delay)
            self.assertIsNone(self.abort_msg)
        self.publish_pressure(pressure_threshold + 1)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.abort_msg)

    def test_pressure_timeout(self):
        pressure_timeout = rospy.get_param("/executive/pressure_abort_timeout")
        pressure_threshold = rospy.get_param("/executive/maximum_pressure")
        self.publish_pressure(0.9 * pressure_threshold)
        t0 = rospy.Time.now().to_sec()
        # Pet the leak watchdog while we wait for the pressure watchdog to trigger
        while rospy.Time.now().to_sec() < t0 + pressure_timeout - self.rosmsg_delay:
            self.publish_leak(False)
            rospy.sleep(self.rosmsg_delay)
            self.assertIsNone(self.abort_msg)
        # Make sure we expect the pressure watchdog to have triggered
        while rospy.Time.now().to_sec() < t0 + pressure_timeout + self.rosmsg_delay:
            rospy.sleep(self.rosmsg_delay)
            # Don't check abort_msg, because at some point in this loop it should trigger
        self.assertIsNotNone(self.abort_msg)

    def test_leak_timeout(self):
        leak_timeout = rospy.get_param("/executive/leak_abort_timeout")
        pressure_threshold = rospy.get_param("/executive/maximum_pressure")
        self.publish_leak(False)
        t0 = rospy.Time.now().to_sec()
        # Pet the pressure watchdog while we wait for the leak watchdog to trigger
        while rospy.Time.now().to_sec() < t0 + leak_timeout - self.rosmsg_delay:
            self.publish_pressure(0.9 * pressure_threshold)
            rospy.sleep(self.rosmsg_delay)
            self.assertIsNone(self.abort_msg)
        # Make sure we expect the leak watchdog to have triggered
        while rospy.Time.now().to_sec() < t0 + leak_timeout + self.rosmsg_delay:
            rospy.sleep(self.rosmsg_delay)
            # Don't check abort_msg, because at some point in this loop it should trigger
        self.assertIsNotNone(self.abort_msg)


if __name__ == "__main__":
    import rostest

    rospy.init_node("test_leak_response")

    rostest.rosrun("backseat_executive", "test_leak_response", TestLeakResponse)
