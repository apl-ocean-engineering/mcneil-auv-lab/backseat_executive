#! /usr/bin/env python3

import rospy
import unittest

import apl_msgs.msg
import remus_backseat_msgs.msg


class TestPowerManagement(unittest.TestCase):
    def setUp(self):
        # Seconds to sleep while waiting for ROS messages to be sent/received
        self.rosmsg_delay = 0.5
        self.power_channel = rospy.get_param("/executive/pump_power_channel")
        self.power_msg = None
        self.power_dict = None
        self.power_sub = rospy.Subscriber(
            "/executive/power_command",
            apl_msgs.msg.PowerCommand,
            self.power_command_callback,
        )
        self.recon_state_pub = rospy.Publisher(
            "/executive/V2RState", remus_backseat_msgs.msg.V2RState, queue_size=1
        )
        self.power_status_pub = rospy.Publisher(
            "/executive/power_status", apl_msgs.msg.PowerStatus, queue_size=1
        )
        rospy.sleep(3.0)  # Give nodes time to connect

    def power_command_callback(self, msg):
        self.power_msg = msg
        self.power_dict = {
            kk: msg.ON == vv for kk, vv in zip(msg.channels, msg.commands)
        }

    def publish_recon_state(self, mode: int) -> None:
        msg = remus_backseat_msgs.msg.V2RState()
        msg.header.stamp = rospy.Time.now()
        msg.mode = mode
        self.recon_state_pub.publish(msg)

    def publish_power_status(self, status: bool) -> None:
        msg = apl_msgs.msg.PowerStatus()
        msg.header.stamp = rospy.Time.now()
        msg.channels = [self.power_channel]
        msg.status = [status]
        self.power_status_pub.publish(msg)

    def test_nominal_mission(self):
        # state starts in 0b00; if power already off, don't expect any messages.
        self.publish_power_status(False)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b00)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNone(self.power_msg)

        # State then transitions to 0b01
        self.publish_power_status(False)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b01)
        rospy.sleep(self.rosmsg_delay)
        # Check for None so this test fails rather than throwing an error
        # on the next call
        self.assertIsNotNone(self.power_dict)
        self.assertTrue(self.power_dict[self.power_channel])
        self.power_msg = None  # Reset
        self.power_dict = None

        # And, transitions to 0b11
        self.publish_power_status(True)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b11)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.power_dict)
        self.assertFalse(self.power_dict[self.power_channel])

    def test_dropped_message(self):
        """
        The Executive should re-send a command if the most recently reported
        PowerStatus doesn't match the desired state given the most recent
        Recon-reported vehicle state
        """
        # NOTE: For this test, status needs to be published before remus
        #   state, because it's the state callbacks that trigger a behavior.
        #   In the actual vehicle, state is published at 9Hz.
        # TODO(lindzey): Consider having the test fixture publish
        #   state constantly via a timer callback, and the tests simply
        #   change the value that's being published?

        # state starts in 0b00; if power already off, don't expect any messages.
        self.publish_power_status(False)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b00)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNone(self.power_msg)
        # Now send message indicating that power is NOT as expected
        self.publish_power_status(True)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b00)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.power_dict)
        self.assertFalse(self.power_dict[self.power_channel])
        self.power_dict = None  # Reset
        self.power_msg = None

        # State then transitions to 0b01
        self.publish_power_status(False)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b01)
        rospy.sleep(self.rosmsg_delay)
        # Check for None so this test fails rather than throwing an error
        # on the next call
        self.assertIsNotNone(self.power_dict)
        self.assertTrue(self.power_dict[self.power_channel])
        self.publish_power_status(True)
        self.power_msg = None  # Reset
        self.power_dict = None
        # Next, check that no additional commands are sent
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b01)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNone(self.power_dict)
        # Now send message indicating that power is still off; executive
        # should try to turn it on.
        self.publish_power_status(False)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b01)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.power_dict)
        self.assertTrue(self.power_dict[self.power_channel])
        self.publish_power_status(True)
        self.power_msg = None  # Reset
        self.power_dict = None

        # And, transitions to 0b11
        self.publish_power_status(True)
        rospy.sleep(self.rosmsg_delay)
        self.publish_recon_state(0b11)
        rospy.sleep(self.rosmsg_delay)
        self.assertIsNotNone(self.power_dict)
        self.assertFalse(self.power_dict[self.power_channel])


if __name__ == "__main__":
    import rostest

    rospy.init_node("test_power_management")

    rostest.rosrun("backseat_executive", "test_power_management", TestPowerManagement)
